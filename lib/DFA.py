

from collections import defaultdict
from random import *
from itertools import *
from functools import *

from util import *


# Here, the arguments have the following signatures:
#    states : FinSet (or similar)
#    lang   : FinSet (or similar)
#    trans  : states => (lang => states)
#    start  : states
#    term   : P(states)

class DFA:
	def __init__(self, states, lang, trans, start, term):
		self.states = set(states)
		self.lang   = set(lang)
		self.trans  = trans
		self.start  = start
		self.term   = set(term)
		self.state  = None
		self.stateT = dict()
		self.termU  = "terminator"
		self.termUS = set([self.termU])
	
		if start not in self.states:
			raise "start not in states"
		
	def reset(self):
		self.state  = self.start
		self.stateT = self.trans.get(self.state, dict())
		
	def __contains__(self, item):
		if item in self.lang: return item in self.stateT
		return False
	
	def next(self, l):
		if l in self:
			self.state = self.stateT[l]
			self.stateT = self.trans[self.state]
			return True
		return False
	
	def hasNext(self, l): return l in self
	
	def domain(self): return set(self.stateT.keys())
	
	def domainT(self):
		termset = self.termUS if self.terminated() else set()
		return self.domain() | termset
	
	def terminated(self): return self.state in self.term
	
	def deadlocked(self): return self.domain() == set([])
	
	def recognize(self, w):
		self.reset()
		for l in w:
			if not self.next(l): return False
		return self.terminated()
	
	def trace(self, w):
		self.reset()
		tr = list([self.state])
		for l in w:
			if not self.next(l): return (False, tr)
			tr.append(self.state)
		return (self.terminated(), tr)
	
	def generate(self, limit = 1000000):
		self.reset()
		w = []
		for n in range(limit):
			l = selFrom(self.domainT())
			if l is self.termU: return w
			w.append(l)
			self.next(l)
		return w
	
	def prefixClose(self):
		self.term = self.states
	
	# Make a linear automata from
	#    w : list
	@staticmethod
	def makeLinearTrans(w):
		start, term = 0, len(w)
		states = set(range(term+1))	
		trans  = {n: {l: n+1} for n, l in enumerate(w)}
		trans[term] = dict()
		return DFA(states, set(w), trans, start, [term])
	
	# Make a linear automata from
	#    w : list
	@staticmethod
	def makeLinearStates(w):
		start, term = '0', w[-1] 
		states = '0' + w
		trans  = {l: {l+'-'+k: k} for l, k in zip(states, w)}
		lang   = {l+'-'+k for l, k in zip(states, w)}
		trans[term] = dict()
		return DFA(states, lang, trans, start, [term])

