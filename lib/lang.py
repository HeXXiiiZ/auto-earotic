
# -*- coding: latin-1 -*-

from itertools import *
from functools import *
import random

from music21 import note, stream, chord

from util import *


################################################################################
## Utilities
################################################################################

def SetConsts(name, attr):
	for k, v in attr.items():
		v.idl, v.idc = k, name
		v.idg = v.idc + '_' + v.idl
		globals()[v.idg] = v
	keysl = list(attr.keys())
	valsl = list(attr.values())
	def items(self): return ((k, attr[k]) for k in keysl)
	
	attr['__keys__'] = keysl 
	attr['__iter__'] = keysl.__iter__
	attr['items'] = items
	attr['values'] = lambda self: valsl
	attr['words'] = lambda self, s: self[words(s)]

	def itergetter(self, key):
		if isinstance(key, list): 
			return map(lambda k: itergetter(self, k), key)
		return attr[key]
	attr['__getitem__'] = itergetter
	
	ks = type(name, (), attr)
	ks_inst = ks()
	globals()[name] = ks_inst
	return ks_inst


# File-wide metaclass for memoizing and defaulting pretty printing.
def instMemo(name, parents, attr):
	inst = dict()
	checker = None 
	if 'signature' in attr:
		signa = attr['signature']
		checker = lambda args: paramCheck(signa, args) 
	
	def newfun(cls, *args):
		if checker: args = checker(args)
		key = repr(args) 
		if 'normalize' in attr: key = attr['normalize'](*args)
		if key in inst: return inst[key]
		newobj = object.__new__(cls)
		inst[key] = newobj
		return newobj 
	
	def namer(inst): return inst.name
	
	def immutor(self, name, value):
		if not hasattr(self, name): self.__dict__[name] = value
	
	attr['__new__']   = newfun
	attr['instDict']  = inst 
	attr['__str__']   = namer 
	attr['__repr__']  = namer
	attr['__setattr__'] = immutor
	
	cls = type(name, parents, attr)
	
	if checker: 
		initer = cls.__init__
		cls.__init__ = lambda self, *args: initer(self, *checker(args)) 
	
	return cls

__metaclass__ = instMemo


################################################################################
## Base Sets
################################################################################

## The notes without accidentals
## The rootstock of diatonic music

kTones = ['C', 'D', 'E', 'F', 'G', 'A', 'B']
kTonesR = revDict(kTones)
kTonesDist = [0, 2, 4, 5, 7, 9, 11]

def diatoneNorm(dex = 0):
	return dex % len(kTones) if type(dex) is int else kTonesR.get(dex, 0)

class Diatone:
	normalize = diatoneNorm 
	
	def __init__(self, dex = 0):
		self.index = diatoneNorm(dex) 
		self.dist  = kTonesDist[self.index]
		self.name  = kTones[self.index]
	
	def __add__(self, sm):
		if isinstance(sm, Diadex):   return Diatone(self.index + sm.index)
		if isinstance(sm, Shift):    return Note(self, sm)
		if isinstance(sm, Index):    return Note(self) + sm
		if isinstance(sm, Octave):   return Note(self) + RelVoice(sm)
		if isinstance(sm, RelVoice): return Note(self) + sm
		return NotImplemented 
	
	def __sub__(self, df):
		if isinstance(df, Diatone): return Diadex((self.index - df.index) % 7)
		if isinstance(df, Diadex):  return Diadex((self.index - df.index) % 7)
		return None
	
	def __cmp__(self, b): return cmp(self.index, b.index)
	
	@classmethod
	def random(self):
		return Diatone(randint(0, len(kTones)-1))
	
	def diff(self, b):
		if not isinstance(b, Diatone): return None
		if self == b: return True
		return (self, b)

################################################################################
## Accidentals (in other words)

kShiftsSyn  = { '' : 0, 'n' : 0, 'b' : -1, 'bb' : -2, 's' : 1, 'ss' : 2 }
kShifts     = { u'♭' : -1, u'♮' : 0, u'♯' : 1 } 
kShiftsSynR = revDict(kShiftsSyn)
kShiftsR    = revDict(kShifts)

def shiftName(x): return abs(x)*('#' if x >= 0 else '-')

class Shift:
	def __init__(self, shift = 0):
		if   shift in kShiftsSyn: shift = kShiftsSyn[shift]
		elif shift in kShifts:    shift = kShifts[shift] 
		elif not (type(shift) is int): shift = 0
		
		self.shift = shift
		self.name  = shiftName(shift)
	
	def __add__(self, b): 
		if isinstance(b, Shift):    return Shift(self.shift + b.shift)
		if isinstance(b, Diadex):   return b + self
		if isinstance(b, Index):    return b + self
		if isinstance(b, Octave):   return Index(self) + RelVoice(b)
		if isinstance(b, RelVoice): return Index(self) + RelVoice(b)
		return NotImplemented 
	
	def __cmp__(self, b): return cmp(self.shift, b.shift)
	
	@classmethod
	def random(self):
		return Shift(randint(-1, 1))
	
	def diff(self, b):
		if not isinstance(b, Shift): return None
		if self == b: return True
		return self.shift - b.shift


################################################################################
## Relative index bases of the major scale 

rkDegU = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII'] 
rkDegL = ['i', 'ii', 'iii', 'iv', 'v', 'vi', 'vii'] 

diaDegs = words("i1 i2 i3 i4 i5 i6 i7")
diaRel  = words("r0 r1 r2 r3 r4 r5 r6")
diaDegsR = revDict(diaDegs)
diaRelR  = revDict(diaRel)

kTonalDeg = { 0, 3, 4 }
kModalDeg = { 1, 2, 5, 6 }


def diadexNorm(dex = 0): return dex % len(kTones)

class Diadex: 
	normalize = diadexNorm
	
	def __init__(self, dex = 0):
		if   dex in diaDegs: dex = diaDegsR[dex]
		elif dex in diaRel:  dex = diaRelR[dex]
		elif not (type(dex) is int): dex = 0 
		
		self.index = dex % 7
		self.dist  = kTonesDist[self.index]
		self.name  = diaDegs[self.index]
		self.tonal = self.index in kTonalDeg
		self.modal = self.index in kModalDeg
	
	def __add__(self, b): 
		if isinstance(b, Shift):    return Index(self, b)
		if isinstance(b, Diadex):   return Diadex(self.index + b.index)
		if isinstance(b, Index):    return Index(self) + b  
		if isinstance(b, Octave):   return RelVoice(self) + RelVoice(b)
		if isinstance(b, RelVoice): return RelVoice(self) + b
		return NotImplemented 
	 
	def __sub__(self, dt):
		if isinstance(dt, Diadex):  return Diadex(self.index - dt.index)
		return NotImplemented
	
	def __cmp__(self, b): return cmp(self.index, b.index)


################################################################################
## Simple integer octaves

class Octave: 
	def __init__(self, dex = 0):
		self.index = dex 
		self.name = repr(dex)
	
	def __cmp__(self, b): 
		return cmp(self.index, b.index)
	
	def __add__(self, b): 
		if isinstance(b, Shift):    return Index(b) + RelVoice(self)
		if isinstance(b, Diadex):   return Index(b) + self 
		if isinstance(b, Index):    return b + self 
		if isinstance(b, Octave):   return Octave(self.index + b.index)
		if isinstance(b, RelVoice): return RelVoice(self) + b
		return NotImplemented 
	
	def __sub__(self, b): return Octave(self.index - b.index)




################################################################################
## Note and Relative Note composites 
################################################################################

class Index:
	signature = ((Diadex, Diadex()), (Shift, Shift()))
	
	def __init__(self, diadex, shift):
		self.diadex  = diadex 
		self.shift   = shift 
		self.dist    = diadex.dist + shift.shift
		self.name    = str(diadex) + str(shift)
		self.tonal   = diadex.tonal
		self.modal   = diadex.modal
	
	def __hash__(self): return hash(self.name)
	
	def __cmp__(self, b): 
		if self.diadex == b.diadex: return cmp(self.shift, b.shift)
		return cmp(self.diadex, b.diadex)
	
	def __add__(self, b):
		if isinstance(b, Shift):    return self + Index(b) 
		if isinstance(b, Diadex):   return self + Index(b) 
		if isinstance(b, Index):
			diadex = self.diadex + b.diadex
			dist = self.dist + b.dist
			if diadex < self.diadex: dist -= 12
			return Index(diadex, Shift(dist - diadex.dist))
		if isinstance(b, Octave):   return RelVoice(self) + b
		if isinstance(b, RelVoice): return RelVoice(self) + b
		if isinstance(b, Interval): return RelVoice(self) + b
		return None
	
	def __sub__(self, b):
		if isinstance(b, Index):
			diadex = self.diadex - b.diadex
			dist   = self.dist - b.dist
			if self.diadex < b.diadex: dist += 12
			return Index(diadex, Shift(dist - diadex.dist))
		return NotImplemented


################################################################################
class Note:
	signature = ((Diatone, Diatone()), (Shift, Shift()))
	
	def __init__(self, diatone, shift):
		self.diatone = diatone
		self.shift   = shift
		self.dist    = diatone.dist + shift.shift
		self.name    = str(diatone) + str(shift)
	
	def __add__(self, b):
		if isinstance(b, Shift):    return self + Index(b) 
		if isinstance(b, Diadex):   return self + Index(b) 
		if isinstance(b, Octave):   return Voice(self, b)
		if isinstance(b, RelVoice): return Voice(self) + b
		if isinstance(b, Interval): return Voice(self) + b
		if isinstance(b, Index):
			diatone = self.diatone + b.diadex
			dist = self.dist + b.dist
			if diatone < self.diatone: dist -= 12
			return Note(diatone, Shift(dist - diatone.dist))
		return None
	
	def __sub__(self, df):
		if isinstance(df, Note):
			diadex = self.diatone - df.diatone
			dist = self.dist - df.dist
			if dist < 0: dist += 12
			return Index(diadex, Shift(dist - diadex.dist))
		return None
	
	def __cmp__(self, b): 
		if self.diatone == b.diatone: return cmp(self.shift, b.shift)
		return cmp(self.diatone, b.diatone)
	
	@classmethod
	def random(self):
		return Note(Diatone.random(), Shift.random())
	
	def diff(self, b):
		if not isinstance(b, Note): return None
		if self == b: return True
		
		diffs = dict()
		if self.diatone != b.diatone: 
			diffs['tone'] = self.diatone.diff(b.diatone)
		if self.shift != b.shift:
			diffs['shift'] = self.shift.diff(b.shift)
		
		return diffs



################################################################################
class RelVoice:
	signature = ((Index, Index()), (Octave, Octave()))
	
	def __init__(self, index, octave):
		self.index   = index 
		self.octave  = octave
		self.name    = str(index) + '+' + str(octave)
	
	def __add__(self, b):
		if isinstance(b, Shift):    return self + Index(b) 
		if isinstance(b, Diadex):   return self + Index(b) 
		if isinstance(b, Index):    return self + RelVoice(b)
		if isinstance(b, Octave):   return self + RelVoice(b)
		if isinstance(b, Interval): return self + RelVoice(b.index, b.octave)
		if isinstance(b, RelVoice): 
			index = self.index + b.index
			offs = Octave(0 if self.index.diadex <= index.diadex else 1)
			octave = self.octave + b.octave + offs
			return RelVoice(index, octave)
		return NotImplemented
	
	def __sub__(self, b):
		if isinstance(b, RelVoice):
			index = self.index - b.index
			offs = Octave(0 if self.index.diadex >= b.index.diadex else -1)
			octave = self.octave - b.octave + offs
			return RelVoice(index, octave)
		return NotImplemented

class Voice:
	def __init__(self, note, octave = Octave()):
		self.note    = note 
		self.octave  = octave
		self.name    = str(note) + str(octave)
	
	def __cmp__(self, b): 
		if self.octave == b.octave: return cmp(self.note, b.note)
		return cmp(self.octave, b.octave)
	
	def __add__(self, b):
		if isinstance(b, Shift):    return self + Index(b) 
		if isinstance(b, Diadex):   return self + Index(b) 
		if isinstance(b, Index):    return self + RelVoice(b)
		if isinstance(b, Octave):   return self + RelVoice(b)
		if isinstance(b, Interval): return self + RelVoice(b.index, b.octave)
		if isinstance(b, RelVoice): 
			note = self.note + b.index
			offs = Octave(0 if self.note.diatone <= note.diatone else 1)
			octave = self.octave + b.octave + offs
			return Voice(note, octave)
		return NotImplemented
	
	def __sub__(self, b):
		if isinstance(b, Voice):
			index = self.note - b.note
			offs  = Octave(0 if self.note.diatone >= b.note.diatone else -1)
			octave = self.octave - b.octave + offs
			return RelVoice(index, octave)
		
	def notate(self):
		n = note.Note(self.name)
		n.duration.type = "quarter"
		return n








################################################################################
## Scales 
################################################################################

################################################################################
## Scale Cardinality 

class ScaleCard:
	def __init__(self, n):
		self.card = n
		self.name = repr(n)
		self.degdices = map(self.degdex, range(n))
		self.degrees  = map(self.degree, range(n))
	
	def __len__(self): return self.card
	
	def __getitem__(self, key):
		if isinstance(key, int): return Degdex(self, key)
	
	def degdex(self, key): return Degdex(self, key)
	
	def degree(self, key): return Degree(self.degdex(key))
	




################################################################################
## Scales

def scaleTypeNorm(indices, name): 
	return repr(indices) 


class ScaleType:
	normalize = scaleTypeNorm
	
	def __init__(self, indices, name = ""):
		self.indices = indices
		self.name    = name
		self.card    = ScaleCard(len(indices)) 
	
	def __len__(self): return len(self.card)
	
	def __getitem__(self, key):
		if isinstance(key, int): 
			return self[self.card[key]]
		if isinstance(key, Degdex) and key in self: 
			return self.indices[key.index]
		if isinstance(key, Degree) and key in self: 
			return self[key.degdex] + key.shift
		if isinstance(key, Placement) and key in self:
			return self[key.degree] + key.octave 
		return NotImplemented
	
	def __contains__(self, item): 
		if isinstance(item, Index):  return item in self.indices
		if isinstance(item, Degdex): return self.card == item.card 
		if isinstance(item, Degree): return item.degdex in self
		if isinstance(item, Placement): return item.degree in self
		return False
	
	def __div__(self, els):
		if isinstance(els, ChordFamily): return self/els.placements
		if len(els) < 1: return ScaleType(list(), "empty")
		if all([isinstance(e, Degdex) or isinstance(e, Degree) for e in els]):
			indices = [self[e] for e in els]
			return ScaleType(indices, "")
		if all([isinstance(e, Placement) for e in els]):
			first, rest = els[0], els[1:]
			relVoices = [self[e] - self[first] for e in rest]
			intervals = [Interval(r.index, r.octave) for r in relVoices]
			return ChordType(intervals, "")
	
	def hfun(self, els, deg):
		if isinstance(deg, Degdex) and isinstance(els, ChordFamily): 
			return HFunction(self/(els + deg), deg)
		return NotImplemented






################################################################################
sufxUns = words("th st nd rd th th th th th th")
def sufx(index):
	if 10 <= index < 20: return 'th'
	return sufxUns[index%10]

def sufxed(index): return repr(index) + sufx(index)

def degdexNorm(m, index):
	index = index % m.card
	return sufxed(index  + 1) + '/' + repr(m) 

class Degdex:
	normalize = degdexNorm
	
	def __init__(self, m, index = 0):
		index = index % m.card
		self.index = index 
		self.card  = m 
		self.name  = sufxed(index + 1) + '/' + repr(m)
	
	def __cmp__(self, b): return cmp(self.index, b.index)
	
	def __add__(self, b): 
		if isinstance(b, Degdex) and self.card == b.card:
			return Degdex(self.card, (self.index + b.index)%self.card.card)
		if isinstance(b, Degree) and self.card == b.card:
			return Degree(self) + b
		if isinstance(b, Octave): return Degree(self) + b
		return NotImplemented

class Degree:
	def __init__(self, degdex, shift = Shift()):
		self.degdex = degdex
		self.shift  = shift
		self.card   = degdex.card
		self.name   = sufxed(degdex.index + 1) + repr(shift) + '/' + repr(degdex.card)
	
	def __cmp__(self, b):
		if self.degdex == b.degdex: return cmp(self.shift, b.shift)
		return cmp(self.degdex, b.degdex)
	
	def before(self, b): return self.degdex < b.degdex
	
	def __add__(self, b): 
		if isinstance(b, Degdex) and self.card == b.card:
			return self + Degree(b)
		if isinstance(b, Degree) and self.card == b.card:
			return Degree(self.degdex + b.degdex, self.shift + b.shift)
		if isinstance(b, Octave):
			return Placement(self, b)
		return NotImplemented

class Placement:
	def __init__(self, deg, octave = Octave(0)):
		self.degree = deg 
		self.octave = octave
		self.name = repr(deg) + '@' + repr(octave) 
	
	def __cmp__(self, b):
		if self.octave != b.octave: return cmp(self.octave, b.octave) 
		return cmp(self.degree, b.degree)
	
	def __add__(self, b):
		if isinstance(b, Octave): 
			return Placement(self.degree, self.octave + b)
		if isinstance(b, Placement):
			degree = self.degree + b.degree
			offs = Octave(0 if self.degree.degdex <= degree.degdex else 1)
			octave = self.octave + b.octave + offs
			return Placement(degree, octave)
		if isinstance(b, Degree): return self + Placement(b)
		if isinstance(b, Degdex): return self + Degree(b)
		return NotImplemented
	
	def place(self, scale, voice):
		note = scale[self.degree]
		if isinstance(note, Index): note = voice.note + note
		octcorr = 0
		if note < voice.note: octcorr = 1
		return Voice(note, Octave(voice.octave.index + self.octave.index + octcorr))

def quickPlacement(cardn, octn, degn, shiftn = 0):
	deg = Degdex(ScaleCard(cardn), degn)
	return Placement(Degree(deg, Shift(shiftn)), Octave(octn))




################################################################################
## Keys / Scales-proper


class Scale:
	def __init__(self, st, note):
		self.scaleType = st
		self.root = note
		self.name = repr(note) + '-' + repr(st)
	
	def __len__(self): return len(self.scaleType)
	
	def __getitem__(self, key):
		index = self.scaleType[key]
		if index is None: return None
		return self.root + index
	
	def __contains__(self, item): 
		if isinstance(item, Note): return (item - self.root) in self.scaleType
		return item in self.scaleType
	
	def notateScale(self):
		notes = map(lambda x: self.root + x, self.scaleType.indices)
		s = stream.Stream()
		for n in notes: s.append(note.Note(str(n) + '4'))
		return s



################################################################################
## Chords 
################################################################################

## Intervals

kMods = { 'd': -1, 'm': 0, 'P': 0, 'M': 0, 'A': 1 }
kDropMods = { 'd', 'm' }
kDropDegs = { 1, 2, 5, 6 }

def resolveMod(diadex, mod): 
	if mod not in kMods: return 0
	return kMods[mod] - (1 if mod in kDropMods and diadex.index in kDropDegs else 0)

def inferMod(index):
	shift = index.shift.shift
	if index.modal:
		if shift ==  0: return 'M'
		if shift == -1: return 'm'
		if shift >   0: return shift*'A'
		return (-shift-1)*'d'
	else:
		if shift == 0: return 'P'
		return abs(shift)*('A' if shift > 0 else 'd') 


class Interval:
	def __init__(self, index, octave = Octave(0)):
		self.index   = index
		self.tonal   = index.tonal
		self.modal   = index.modal
		self.octave  = octave
		self.dist    = index.diadex.index + octave.index*7
		self.family  = index.diadex.index + 7*octave.index
		self.name    = inferMod(index) + str(self.family + 1)
		self.relVoice = RelVoice(index, octave)
	
	def __cmp__(self, b):
		if self.octave == b.octave: return cmp(self.index, b.index)
		return cmp(self.octave, b.octave)
	
	def __add__(self, b):
		if isinstance(b, Interval): 
			return Interval(self.index + b.index, self.octave + b.octave)
		if isinstance(b, Octave):
			return Interval(self.index, self.octave + b)
		return NotImplemented
	
	def diff(self, b): 
		if not isinstance(b, Interval): return None
		if self == b: return True
		
		diffs = dict()
		if self.family != b.family:
			if (self.family%7) == (b.family%7):
				diffs['octave'] = (self.family - b.family)%7
			else: diffs['family'] = (self.family, b.family)
		if self.index.shift != b.index.shift:
			diffs['shift'] = self.index.shift.diff(b.index.shift)
		
		return diffs


################################################################################
class ChordFamily:
	def __init__(self, placements):
		self.placements = placements
		self.card = ScaleCard(len(placements))
		self.name = repr(placements)
	
	def __len__(self): return len(self.placements)
	
	def __add__(self, b):
		if isinstance(b, Degdex): 
			return ChordFamily([p + b for p in self.placements])
		return NotImplemented




def chordTypeNorm(intervals, code = "undef", name = "undef"): 
	return repr(intervals) 

class ChordType:
	normalize = chordTypeNorm
	
	def __init__(self, intervals, code = "undef", name = "undef"):
		self.intervals = intervals
		if not hasattr(self, 'name'): self.name = name
		if not hasattr(self, 'code'): self.code = code
		self.card = ScaleCard(len(intervals) + 1)
		self.defined = self.name != "undef"
	
	def __len__(self): return len(self.intervals)
	
	def __contains__(self, item):
		if isinstance(item, Component): return item.chordLen == len(self)
		if isinstance(item, Degdex):    return item.card == self.card
		if isinstance(item, Degree):    return item.degdex in self
		if isinstance(item, Placement): return item.degree in self
		if isinstance(item, Interval):  return item in intervals
		return False
	
	def __getitem__(self, key):
		if isinstance(key, Component) and key in self: return self.intervals[key.index]
		if isinstance(key, Degdex):     
			if key.index == 0: return RelVoice(Indices.i1) 
			return self.intervals[key.index-1].relVoice
		if isinstance(key, Degree):    return self[key.degdex] + key.shift 
		if isinstance(key, Placement): return self[key.degree] + key.octave
		return None
	
	def diff(self, b):
		if not isinstance(b, ChordType): return None
		if self == b: return True
		
		diffs = dict()
		intvs = set(b.intervals)
		offs, missing = list(), list()
		for i in self.intervals:
			for k in intvs:
				if i == k:
					intvs.remove(k)
					break
				if i.family == k.family:
					offs.append((i, k))
					intvs.remove(k)
					break
			else: missing.append(i)
		
		if len(intvs)   > 0: diffs['extra']   = list(intvs)
		if len(offs)    > 0: diffs['off']     = offs
		if len(missing) > 0: diffs['missing'] = missing
		return diffs



################################################################################
class HFunction:
	def __init__(self, ct, deg):
		self.chordType = ct
		self.degree    = deg 
		self.name      = '[' + repr(deg) + ']' + repr(ct)
	
	def __len__(self): return len(self.chordType)
	
	def __call__(self, st):
		if isinstance(st, ScaleType):
			return RelChord(self.chordType, st[self.degree])
		if isinstance(st, Scale):
			return Chord(self.chordType, st[self.degree])



################################################################################
class Component:
	def __init__(self, index, clen = 1):
		self.chordLen = clen
		self.index = index % clen 
		self.name  = sufxed(self.index + 1) + '/' + repr(clen)

################################################################################
class RelChord:
	def __init__(self, ct, index):
		self.chordType = ct
		self.index = index 
		self.name  = repr(index) + ' ' + repr(ct)
	
	def __len__(self): return len(self.chordType)
	
	def __contains__(self, item):
		if isinstance(item, Index): 
			return (item - self.index) in self.chordType
		return item in self.chordType
	
	def __getitem__(self, key):
		if key in self.chordType: 
			return self.index + self.chordType[key]
	
	def __add__(self, b):
		if isinstance(b, Note):  
			return Chord(self.chordType, b + self.index)
		if isinstance(b, Index): 
			return RelChord(self.chordType, b + self.index)
		return NotImplemented

################################################################################
class Chord:
	def __init__(self, ct, note):
		self.chordType = ct
		self.note = note
		self.notes = self.getNotes()
		self.card = ct.card
		self.name = repr(note) + ' ' + repr(ct)
	
	def __len__(self): return len(self.chordType)
	
	def __contains__(self, item):
		if isinstance(item, Note): return (item - self.note) in self.chordType
		return item in self.chordType
	
	def __getitem__(self, key):
		if key in self.chordType: return self.note + self.chordType[key] 
	
	def __add__(self, b):
		if isinstance(b, Index):
			return Chord(self.chordType, self.note + b)
	
	def getNotes(self):
		card = self.chordType.card
		return [self[Degdex(card, n)] for n in range(len(self))]
	
	def diff(self, b):
		if not isinstance(b, Chord): return None 
		if self == b: return True
		
		diffs = dict()
		if self.note != b.note: 
			diffs['note'] = self.note.diff(b.note) 
		if self.chordType != b.chordType:
			diffs['chordType'] = self.chordType.diff(b.chordType)
		return diffs


################################################################################
## Voicing 
################################################################################


class InversionType:
	def __init__(self, degree):
		self.root = degree 
		self.name = "root" if degree.index == 0 else sufxed(degree.index) + "-inv"
	


################################################################################
class Inversion:
	def __init__(self, chord, invtype):
		self.chord = chord
		self.inversionType = invtype
		self.name = repr(chord) + '.' + repr(invtype)


def mkRoot(chord):
	return Inversion(chord, InversionType(Degree(0, len(chord.chordType))))



################################################################################
class RelInversion:
	def __init__(self, hfunction, invtype):
		self.hfunction = hfunction 
		self.inversionType = invtype
	


################################################################################
class Layout:
	def __init__(self, placements):
		self.placements = placements
		self.name = repr(self.placements)
	
	def __call__(self, chord): 
		if isinstance(chord, Chord):
			return Voicing(self, chord)

def quickLayout(chordLen, degs):
	placements = map(lambda (x, y): quickPlacements(chordLen, y, x), degs)
	return Layout(placements)


################################################################################
class Voicing:
	def __init__(self, layout, chord, octave = Octave()):
		self.layout = layout 
		self.chord  = chord 
		self.octave = octave
		self.voices = self.getVoices()
		self.name   = repr(self.voices) 
	
	def getVoices(self):
		note, ct = self.chord.note, self.chord.chordType
		return [note + ct[p] + self.octave for p in self.layout.placements]
	
	def notate(self):
		pnotes = [v.notate() for v in self.voices]
		ch = chord.Chord(pnotes)
		ch.duration.type = 'whole'
		return ch
	
	def __add__(self, b):
		if isinstance(b, Octave):
			return Voicing(self.layout, self.chord, self.octave + b)


################################################################################
class RelVoicing:
	def __init__(self, layout, rinversion):
		self.layout = layout 
		self.relInversion = rinversion
		self.name = "Voices"

################################################################################
################################################################################
################################################################################
################################################################################
## Constants


SetConsts('Diatones', {v: Diatone(v) for v in kTones})

SetConsts('Shifts', {k: Shift(v) for k, v in kShiftsSyn.items()})

SetConsts('Diadices', {k: Diadex(v) for k, v in diaDegsR.items()}) 

SetConsts('Indices', {dn+sn: Index(dv, sv) 
	for (dn, dv) in Diadices.items() for (sn, sv) in Shifts.items()})

SetConsts('Notes', {dn+sn: Note(dv, sv)
	for (dn, dv) in Diatones.items() for (sn, sv) in Shifts.items()})

classicNotes = {Notes[k] 
	for k in words("C Cs Db D Ds Eb E F Fs Gb G Gs Ab A As Bb B")}

superClassicNotes = {Notes[k] 
	for k in words("C Cs Db D Eb E F Fs G Gs Ab A As Bb B")}

def randClassicNote():
	return selFrom(classicNotes)

def randSuperClassicNote():
	return selFrom(superClassicNotes)


SetConsts('RelVoices', {dn+'_'+repr(n): RelVoice(dv, Octave(n))
	for (dn, dv) in Indices.items() for n in range(8)})


SetConsts('Voices', {dn+repr(n): Voice(dv, Octave(n))
	for (dn, dv) in Notes.items() for n in range(8)})


romandegs = "I II III IV V VI VII"
SetConsts('Degdices', { v: Degdex(ScaleCard(7), n) 
	for n, v in enumerate(words(romandegs)) }) 

SetConsts('Degrees', { dn+sn: Degree(dv, sv)
	for (dn, dv) in Degdices.items() for (sn, sv) in Shifts.items() })



kMajor     = [Indices[i] for i in words("i1 i2 i3 i4 i5 i6 i7")]
kMinor     = [Indices[i] for i in words("i1 i2 i3b i4 i5 i6b i7b")]
kHarmMinor = [Indices[i] for i in words("i1 i2 i3b i4 i5 i6b i7")]
kMelMinor  = [Indices[i] for i in words("i1 i2 i3b i4 i5 i6 i7")]
kMinSev    = [Indices[i] for i in words("i1 i3b i5 i7b")]

SetConsts("ScaleTypes", {
	'Major' : ScaleType(kMajor, 'Major'),
	'Minor' : ScaleType(kMinor, 'Minor'),
	'HarmMinor' : ScaleType(kHarmMinor, 'HarmMinor'),
	'MelMinor'  : ScaleType(kMelMinor,  'MelMinor'),
	'MinSev'    : ScaleType(kMinSev,    'MinSev')
})


def cacheIntervals():
	inters = dict()
	for (k, v), m in product(Diadices.items(), range(2)):
		for s in range(1, -3 if v.modal else -2, -1):
			inter = Interval(Index(v, Shift(s)), Octave(m))
			inters[repr(inter)] = inter 
	return inters

SetConsts('Intervals', cacheIntervals())



def makeChordFamily(ind):
	return (ind[0], ChordFamily([quickPlacement(7, n/7, n%7) for n in ind[1]]))

chordFamilyList = [
	('diad',     (0, 4)),
	('triad',    (0, 2, 4)),
	('seventh',  (0, 2, 4, 6)),
	('ninth',    (0, 2, 4, 6, 8)),
	('eleventh', (0, 2, 4, 6, 8, 10)),
	('six',      (0, 2, 4, 5)),
	('add9',     (0, 2, 4, 8)),
	('six_add9', (0, 2, 4, 5, 8)),
	('sus2',     (0, 1, 4)),
	('sus4',     (0, 3, 4))
]

SetConsts('ChordFamilies', dict(map(makeChordFamily, chordFamilyList)))


chordTyper = lambda (k, s, n): (k, ChordType([Intervals[i] for i in words(s)], k, n))
chordList = [
# Diads
	('P', "P5", 'Fifth'),
	('T', "d5", 'Tritone'),

# Major Family chords 
	('M',   "M3 P5",        'Major'),
	('M7',  "M3 P5 M7",     'Major 7th'),
	('M9',  "M3 P5 M7 M9",  'Major 9th'),

# Dominant Family chords
	('D7',    "M3 P5 m7",     'Dominant 7th'),
	('D9',    "M3 P5 m7 M9",  'Dominant 9th'),
	('D7_6',  "M3 P5 M6 m7",  'Seven Six'),
	('D7_5b', "M3 d5 m7",     'Dominant 7th flat 5'),

# Major Add Chords
	('M_9',  "M3 P5 M9",     'Major Add 9'),
	('M6',   "M3 P5 M6",     'Major 6'),
	('M6_9', "M3 P5 M6 M9",  'Major 6 Add 9'),

# Minor Family chords 
	('m',  "m3 P5",       'Minor'),
	('m7', "m3 P5 m7",    'Minor 7th'),
	('m9', "m3 P5 m7 M9", 'Minor 9th'),
	('r7', "m3 P5 M7",    'Minor-Major 7th'),
	('r9', "m3 P5 M7 M9", 'Minor-Major 9th'),

# Minor Add Chords
	('m_9',  "m3 P5 M9",     'Minor Add 9'),
	('m6',   "m3 P5 M6",     'Minor 6'),
	('m6_9', "m3 P5 M6 M9",  'Minor 6 Add 9'),

# Diminished Family
	('d',   "m3 d5",    'Diminished'),
	('d7',  "m3 d5 d7", 'Diminished 7th'),
	('hd7', "m3 d5 m7", 'Half-Diminished 7th'),

# Augmented Family
	('A',   "M3 A5",    'Augmented'),
	('A7',  "M3 A5 M7", 'Augmented 7th'),

# Suspensions
	('S2',  "M2 P5",  'Suspended 2nd'),
	('S4',  "P4 P5",  'Suspended 4th')
]

SetConsts('ChordTypes', dict(map(chordTyper, chordList)))


SetConsts('RelChords', {sn+dn: RelChord(dv, sv)
	for (dn, dv) in ChordTypes.items() for (sn, sv) in Indices.items()})


SetConsts('Chords', {sn+dn: Chord(dv, sv)
	for (dn, dv) in ChordTypes.items() for (sn, sv) in Notes.items()})






