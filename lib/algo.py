

from itertools import *
from functools import *
from random import *

from util import *
from lang import *
from DFA  import *


################################################################################
## Algorithms 
################################################################################

def genPlacements(cardn, octaves, inv):
	card = ScaleCard(cardn)
	degf = lambda n: Degree(Degdex(card, n))
	bass = degf(inv)
	degs = [degf(n) for n in range(cardn)]
	relocts = lambda d: range(1, octaves+1) if d.before(bass) else range(octaves)
	return {d: {Placement(d, Octave(x)) for x in relocts(d)} for d in degs}

def genBassSep(chordCard, octaves, inv):
	bass = Degree(inv, chordCard)
	degs = (Degree(n, chordCard) for n in range(chordCard))
	p = {d: {Placement(d, Octave(x)) for x in range(1, octaves)} for d in degs}
	p[bass].add(Placement(bass, Octave(0)))
	return p

def genRandLayout(cardn, chordSize, octs, inv = None):
	if inv is None: inv = randint(0, cardn-1)
	
	card = ScaleCard(cardn)
	degf = lambda n: Degree(Degdex(card, n))
	
	#potentials = genPlacements(chordCard, octs, inv)
	potentials = genPlacements(cardn, octs, inv)
	essentials = set(potentials.keys())
	
	bdeg = degf(inv)
	bass = Placement(bdeg, Octave(0))
	
	essentials.remove(bdeg)
	potentials[bdeg].remove(bass) 
	
	placements = [bass] + [drawFrom(potentials[e]) for e in essentials]
	remanders = reduce(lambda a, x: a | x, potentials.values()) 
	
	placements += drawFrom(remanders, chordSize - cardn)
	return Layout(sorted(placements))




# k - range in octaves, W - number of repeated notes, q - inversion
def generateLayoutAlgorithm1(k, W, q, chord):
	bdeg = chord.card.degdex(q)
	octrange = lambda d: (lambda i: range(i, k+i))(int(d < bdeg))
	potset   = lambda d: {d + Octave(x) for x in octrange(d)}
	potentials = {d: potset(d) for d in chord.card.degdices}
	essentials = set(potentials.keys())
	
	bass = bdeg + Octave(0)
	essentials.remove(bdeg)
	potentials[bdeg].remove(bass) 
	
	placements = [bass] + [drawFrom(potentials[e]) for e in essentials]
	
	remanders = reduce(lambda a, x: a | x, potentials.values()) 
	placements += drawFrom(remanders, W)
	return Layout(sorted(placements))


################################################################################

# register : { low : Note, high : Note, count : nat }
# def randRegLayout(chord, registers):
# 	card    = chord.chordType.card
#	degrees = [card[n] for n in range(len(card))]
#	
#	pos = 
    




################################################################################

def randLayoutAlgRange(maxnotes, octs, inversions):
	def layoutAlg(chord):
		cardn = chord.chordType.card.card
		inv = None if inversions else 0 
		layout = genRandLayout(cardn, randint(cardn, maxnotes), octs, inv)
		return layout(chord)
	return layoutAlg

# chordTypes : List(ChordType)
# layout     : Chord -> Voicing 
# notes      : List(Note)
def genChordExample(chordTypes, layoutAlg, notes):
	chordType = selFrom(chordTypes)
	note      = selFrom(notes)
	chord     = Chord(chordType, note)
	voicing   = layoutAlg(chord)
	return voicing 

def checkChordExample(chordExample, chord):
	diff = chordExample.chord.diff(chord)
	return diff 


# Assume chord has only two-octave intervals
def inferChord(voices, inv = False):
	def inferFromRoot(root):
		invset = {Interval(v.note - root) for v in voices if v.note != root}
		invlist = list(invset)
		invlist.sort()
		families = [i.family for i in invlist]
		if all([n in families for n in [2, 4]]):
			for n in [1, 3, 5]:
				if n not in families: break
				i = families.index(n)
				invlist[i] += Octave(1)
			invlist.sort()
		
		return Chord(ChordType(invlist), root) 
	
	if inv:
		noteset = {v.note for v in voices}
		chords  = [inferFromRoot(n) for n in noteset]
		fchords = filter(lambda c: c.chordType.defined, chords)
		if len(fchords) == 1: return fchords[0]
		if len(fchords) >  1: return fchords[0]
		
		return None 
	
	else: 
		voices.sort()
		if len(voices) < 1: return None
		return inferFromRoot(voices[0].note)
	


def checkInference(chord, layoutAlg, inv = False):
	voicing = layoutAlg(chord)
	infchord = inferChord(voicing.voices, inv)
	if infchord == chord: return True
	return (chord, infchord)

def randLayoutAlg(chord):
	cardn = chord.chordType.card.card
	layout = genRandLayout(cardn, randint(cardn, 8), 6, 0) 
	return layout(chord)

def runInferenceCheck(inv = False):
	n = Notes.A
	checker  = lambda v: checkInference(Chord(v, n), randLayoutAlg, inv)
	checkres = map(checker, ChordTypes.values())
	diff     = filter(lambda a: a != True, checkres)
	return diff












