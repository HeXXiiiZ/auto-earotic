

from itertools import *
from functools import *
from random import *


################################################################################
## Utilities  
################################################################################

def words(wl): return wl.split(" ")

def grabConsts(cl, wl): return map(cl.__dict__.get, words(wl))

def join(l, j = " "): 
	return reduce(lambda a, x: a + j + x, l)

def cjoin(l): return join(l, ", ")

def revDict(d):
	items = []
	if type(d) is dict: items = d.items()
	if type(d) is list: items = enumerate(d) 
	return {v: k for k, v in items}

def selOneFrom(s):
	randex = randint(0, len(s) - 1)
	randel = (list(s))[randex]
	return randel

def selFrom(s, n = None):
	if n is None: return selOneFrom(s)
	return [selOneFrom(s) for m in range(n)]

def drawOneFrom(s):
	randel = selOneFrom(s)
	s.remove(randel)
	return randel 

def drawFrom(s, n = None):
	if n is None: return drawOneFrom(s)
	return {drawOneFrom(s) for m in range(n)}

def paramCheck(signa, facts):
	facts = list(facts)
	check = lambda t, l: len(l) > 0 and isinstance(l[0], t)
	params = [facts.pop(0) if check(t, facts) else d for t, d in signa]
	if len(facts) > 0: return NotImplemented
	return params 




